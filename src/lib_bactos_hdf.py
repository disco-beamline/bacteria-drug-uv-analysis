#!/usr/bin/env python
# coding: utf-8

"""
Functions to interact and process data contains in the H5File generated for the
drug accumulation by bacteria experiments done on the DISCO beamline at
Synchrotron-Soleil.
"""

import h5py

def list_experiments(data_in):
    """
    List all experiments contains in the h5file.

    Parameters:
    -----------

    - data_in, str:
        the path to the h5file.

    return a list of experiments names.
    """
    expe = []
    with h5py.File(data_in, 'r') as f:
        for g in f:
            expe += [str(g)]
    return expe


def load_image(data_in, expe_name):
    """
    Get the image stack array for a given experience.

    Parameters:
    -----------

    - data_in, str:
        the path to the h5file.

    - expe_name, str:
        the name of the experiment.


    return a numpy array of the image stack.
    """

    with h5py.File(data_in, 'r') as f:
        output = f[expe_name]['images'][:]

    return output


def load_mask(data_in, expe_name, pos):
    """
    Get the bacteria mask for a given experiment and a given position on the
    image stack.

    Parameters:
    -----------

    - data_in, str:
        the path to the h5file.

    - expe_name, str:
        the name of the experiment.

    - pos, int:
        the position on the image stack.

    return a 2d numpy array.
    """

    with h5py.File(data_in, 'r') as f:
        pos = 'POS%i' % pos
        output = f[expe_name][pos]['bacteria_mask'][:]

    return output

def get_stack_shape(data_in, expe_name):
    """
    Get the dimensions of the image stack for this "expe_name" in the data_in HDF file.

    Parameters:
    -----------

    - data_in, str:
        the path to the h5file.

    - expe_name, str:
        the name of the experiment.

    Return the shape of the image stack
    """
    with h5py.File(data_in, 'r') as f:
        output = f[expe_name]['images'].shape

    return output

def load_intensity_profile(data_in, expe_name, channel, pos):
    """
    Load intensity profile (mean of fluorescence inside each bacteria) for the
    selected experiment, position and channel.

    Parameters:
    -----------

    - data_in, str:
        the path to the h5file.

    - expe_name, str:
        the name of the experiment.

    - channel, int:
        the position of the fluorescence channel in the image stack.

    - pos, int:
        the position on the image stack to use.

    return a list of intensities for each bacteria through time.
    """

    with h5py.File(data_in, 'r') as f:
        pos = 'POS%i' % pos
        output = f[expe_name][pos]['ch%i_bacteria_intensities' % channel][:]

    return output


def load_area(data_in, expe_name, pos):
    """
    Get the area of bacteria.

    Parameters:
    -----------

    - data_in, str:
        the path to the h5file.

    - expe_name, str:
        the name of the experiment.

    - pos, int:
        the position to use in the image stack.

    return an array of bacteria area.
    """

    with h5py.File(data_in, 'r') as f:
        pos = 'POS%i' % pos
        output = f[expe_name][pos]['bacteria_area'][:]

    return output


def save_selected_bactos(data_in, expe_name, pos, selected_bactos):
    """
    Save the user selected bacteria list in the h5file.

    Parameters:
    -----------

    - data_in, str:
        the path to the h5file.

    - expe_name, str:
        the name of the experiment.

    - pos, int:
        the position used in the image stack.

    - selected_bactos, list:
        the list of selected bacteria to save in the h5file.

    """
    with h5py.File(data_in, 'a') as f:
        pos = 'POS%i' % pos
        if 'selected_bactos' in f[expe_name][pos]:
            f[expe_name][pos]['selected_bactos'][:] = selected_bactos
        else:
            f[expe_name][pos].create_dataset('selected_bactos', data=selected_bactos)


def save_drug_signal(data_in, expe_name, pos, drug_data):
    """
    Save the computed drug signal (corrected from TRP crosstalk) to the h5file.

    Parameters:
    -----------

    - data_in, str:
        the path to the h5file.

    - expe_name, str:
        the name of the experiment.

    - pos, int:
        the position in the image stack used.

    - drug_data, array:
        the drug accumulation data to save.
    """

    with h5py.File(data_in, 'a') as f:
        pos = 'POS%i' % pos
        if 'drug_signal' in f[expe_name][pos]:
            del f[expe_name][pos]['drug_signal']

        f[expe_name][pos].create_dataset('drug_signal', data=drug_data)


def load_drug_signal(data_in, expe_name, pos):
    """
    Load the drug accumulation data (corrected from TRP crosstalk) from the
    h5file for a given experiment and position.

    Parameters:
    -----------

    - data_in, str:
        the path to the h5file.

    - expe_name, str:
        the name of the experiment.

    - pos, int:
        the position used on the image stack.
    """

    output = None

    with h5py.File(data_in, 'r') as f:
        pos = 'POS%i' % pos

        if 'drug_signal' in f[expe_name][pos]:
            output = f[expe_name][pos]['drug_signal'][:]

    return output


def load_selected_bactos(data_in, expe_name, pos):
    """
    Get the list of user selected bacteria save in h5file for a given experiment
    and a given position.

    Parameters:
    -----------

    - data_in, str:
        the path to the h5file.

    - expe_name, str:
        the name of the experiment.

    - pos, int:
        the position used on the image stack.

    return a list of selected bacteria id.
    """

    with h5py.File(data_in, 'r') as f:
        pos = 'POS%i' % pos
        if 'selected_bactos' in f[expe_name][pos]:
            output = f[expe_name][pos]['selected_bactos'][:]
        else:
            output = None

    return output


def save_control_exp(data_in, expe_name, control_exp):
    """
    Save the name of the selected control experiment for the experiment.

    Parameters:
    -----------

    - data_in, str:
        the path to the h5file.

    - expe_name, str:
        the name of the experiment.

    - control_exp, str:
        the name of the control experiment.
    """

    with h5py.File(data_in, 'a') as f:
        gp = f[expe_name]
        gp.attrs['control_exp'] = control_exp


def load_control_exp(data_in, expe_name):
    """
    Load a control experiment from the h5file.

    Parameters:
    -----------

    - data_in, str:
        the path to the h5file.

    - expe_name, str:
        the name of the control experiment.
    """

    with h5py.File(data_in, 'r') as f:
        gp = f[expe_name]
        try:
            output = gp.attrs['control_exp']
        except:
            output = None

    return output


def load_filter_positions(data_in, expe_name):
    """
    Load the position of the drug and the trp filter in stack.

    Parameters:
    -----------

    - data_in, str:
        the path to the h5file.

    - expe_name, str:
        the name of the experiment.
    """

    ctrl_filter = 1 # The default position before we add this information to hdf
    drug_filter = 0 # The default position before we add this information to hdf
    with h5py.File(data_in, 'r') as f:
        gp = f[expe_name]
        try:
            ctrl_filter = gp.attrs['ctrl_filter_pos']
            drug_filter = gp.attrs['drug_filter_pos']
        except:
            print('Use pos 0 for drug and pos 1 for controle')

    return ctrl_filter, drug_filter

def load_bacterias_svg_figures(h5file, exp, pos):
    """
    Load svg figures of bacterias
    """

    with h5py.File(h5file, 'r') as f:
        pos = 'POS%i' % pos
        if 'bacteria_svg_figures' in f[exp][pos]:
            output = f[exp][pos]['bacteria_svg_figures'][:]
        else:
            output = None

    return output
