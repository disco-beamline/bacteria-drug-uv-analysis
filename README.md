Scripts used to perform drug uptake analysis by bacteria from Deep-UV images recorded on the TELEMOS microscope at Disco Beamline. 

The current version is in development stage and is currently highly linked to the data acquisition setup used on the DISCO beamline at Synchrotron-Soleil. 

The processing follow the method described by Julia et al. in **Spectrofluorimetric quantification of antibiotic drug concentration in bacterial cells for the characterization of translocation across bacterial membranes** in *Nature Methods 2018*. doi:10.1038/nprot.2018.036

Both: Microspectrofluorimetry (indivudual cells drug concentration) from several position at fixed timepoint and Kinetics microspectrofluorimetry
(KMSF) from several positions and time-points (to get drug accumulation in individual cells) are implemented.

Data are processed (image registration and bacteria segmentation) and stored in an HDF File. The segmentation uses StarDist segmentation algorith trained on the DISCO fluorescent images of bacteria obtain on 100x TELEMOS objective.

A viewver (using jupyter widgets) is then used to analyse the data in the HDF File.

Scripts distributed under the GPL v3. Licence

## Install

Need to install the following python libraries:
- h5py
- numpy (version lower than 1.20)
- pandas
- xlsxwriter
- scikit-image
- matplotlib
- ipywidgets
- ipympl
- tifffile

## Extraction

All will be explained in this notebook:
[Open the "extract-bacteries" notebook](./notebooks/extract-bacteries.ipynb)

Just open it and follow instructions. You may have to adapt the process function to your needs!

## Analyse the processed data
[Open the "View_results" notebook](./notebooks/View_results.ipynb)

