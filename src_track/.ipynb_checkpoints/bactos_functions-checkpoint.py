# coding: utf-8
"""
Python functions to extract bacteria imaged on DISCO beamline at
Synchrotron-Soleil to extract time evolution of fluorescence. Images are first
stabilized then bacteria are extracted using StarDist segmentation algorithm and
finally all data are included inside an HDF5 file.
"""
import re
import json
import glob
import numpy as np
from pathlib import Path
from tifffile import imread
from skimage.feature import match_template, match_descriptors, SIFT
from skimage.filters import gaussian, threshold_yen
from skimage.restoration import denoise_wavelet, estimate_sigma
from skimage.registration import phase_cross_correlation
from skimage.morphology import disk, white_tophat, opening
from skimage.measure import label, regionprops, find_contours, ransac
from skimage.segmentation import clear_border, expand_labels
from skimage import transform, exposure
from skimage.util import img_as_float

from scipy import ndimage as ndi
import dateutil
import h5py
import datetime
import os 
import io

from joblib import Parallel, delayed
from csbdeep.utils import normalize
from stardist.models import StarDist2D, Config2D
from lib_bactos_hdf import load_image, load_mask, load_intensity_profile
import matplotlib.pyplot as plt
from rich.progress import track


def load_MM2_images(image_root_path, channel='*', position='*', time='*', z='*'):
    """
    Load Micro Manager images series recorded as individual files.
    The individual image file format is like:
    img_channel000_position000_time000000000_z000.tif
    
    Parameters:
    -----------
    - image_root_path: string,
        Path to the root folder of images that contains position subfolders (lik
        Pos0, Pos1).
        
    - channel, int or '*',
        Channel number to use. Use '*' to load all images.
        
    - position, int or '*'
        Image poisition, Use '*' to load them all.
    
    - time, int or '*'
        Load a given time. Use '*' to load them all.
        
    - z, int or '*'
        Load a given depth. Use '*' to load them all.

    Return an array with the image collection.
    """
    
    mm2_name_pattern = 'Pos{position}/img_channel{channel:03}_position{position:03}_time{time:09}_z{z:03}.tif'
    reMM2pattern = re.compile('channel([0-9]*)_position([0-9]*)_time([0-9]*)_z([0-9]*)')

    # If we need to only load one image
    if isinstance(channel, int) and isinstance(position, int) and isinstance(time, int) and isinstance(z, int):
        imgp = mm2_name_pattern.format(position=position,
                                      channel=channel,
                                      time=time,
                                      z=z)
        
        output = io.imread(image_root_path+imgp)
        
    else:
        # List all files
        all_channels = glob.glob(image_root_path + 'Pos*')
        print("J'ai trouvé %i positions qui sont: %s" % (len(all_channels), str(all_channels)))
    
        # Read one image to get its dimensions
        all_img_pos0 = glob.glob(all_channels[0]+'/*.tif')
        imtmp = imread(all_img_pos0[0])
        imgshape = imtmp.shape
        imgtype = imtmp.dtype
        print(imgshape, imgtype)
    
        # Read metadata
        all_metadata = tuple(json.load(open(f'{meta_path}/metadata.txt', encoding='latin1')) for meta_path in all_channels)
        meta_data = {}
        for tmp_meta in all_metadata:
            meta_data = meta_data | tmp_meta
            
        # meta_data = json.load(open(all_channels[0]+'/metadata.txt', encoding='latin1'))
        summ = meta_data['Summary']
    
        channel_names = summ['ChNames']
        
        # Create the dimension for the final array
        dimensions = summ['IntendedDimensions']
        maxtime = dimensions['time']
        maxpos = dimensions['position']
        maxz = dimensions['z']
        maxchan = dimensions['channel']

        # Array order should be in the following order; time, pos, z, with, height, channel
        dims = []
        
        if time == '*':
            dims += [maxtime]
            str_time = '*'
        else:
            dims += [1]
            maxtime = 1
            str_time = '%09i' % time
        
        if position == '*':
            dims += [maxpos]
            str_pos = '*'
            str_pos1 = '*'
        else:
            dims += [1]
            maxpos = 1
            str_pos = '%03i' % position
            str_pos1 = '%i' % position
        
        if z == '*':
            dims += [maxz]    
            str_z = '*'
        else:
            dims += [1]
            maxz = 1
            str_z = '%03i' % z
        
        dims += imgshape
        
        if channel == '*':
            dims += [maxchan]
            str_chan = '*'
        else:
            dims += [1]
            maxchan = 1
            str_chan = '%03i' % channel
            

        img_fmt = 'Pos'+str_pos1+'/img_channel'+str_chan+'_position'+str_pos+'_time'+str_time+'_z'+str_z+'.tif'
        selected_imgpaths = glob.glob(image_root_path+img_fmt)
        output = np.empty(dims, dtype=imgtype)

        for imgp in selected_imgpaths:
            tchan, tpos, tt, tz = [int(i) for i in reMM2pattern.findall(imgp)[0]]

            if maxtime == 1:
                tt = 0
            if maxpos == 1:
                tpos = 0
            if maxz == 1:
                tz = 0
            if maxchan == 1:
                tchan = 0
                
            output[tt, tpos, tz, :, :, tchan] = imread(imgp)
                
        
        print(dimensions, channel_names)
        
    return output, meta_data


def load_MM1_images(image_root_path, channel='*', position='*', time='*', z='*'):
    """
    Load MM1 images as numpy array

    Parameters:
    -----------
    - image_root_path: string,
        Path to the root folder of images that contains position subfolders (lik
        Pos0, Pos1).

    - channel, int or '*',
        Channel number to use. Use '*' to load all images.

    - position, int or '*'
        Image poisition, Use '*' to load them all.

    - time, int or '*'
        Load a given time. Use '*' to load them all.

    - z, int or '*'
        Load a given depth. Use '*' to load them all.

    Return an array with the image collection.
    """
    
    mm1_name_pattern = 'Pos{pos}/img_{time:09}_{channel}_{z:03}.tif'
    
    # Read metadata
    with open(image_root_path+'/Pos0/metadata.txt', encoding='latin1') as f:
        meta = json.load(f)
    
    # Get acquisition dimensions
    channels = meta['Summary']['ChNames']
    width = meta['Summary']['Width']
    height = meta['Summary']['Height']
    ntime = meta['Summary']['Frames']
    nslices = meta['Summary']['Slices']
    nPositions = meta['Summary']['Positions']
    nchannels = len(channels)

    if channel == '*':
        loopc = [c for c in range(nchannels)]
    else:
        loopc = [channel]
        nchannels = 1
        
    if position == '*':
        loopp = [p for p in range(nPositions)]
    else:
        loopp = [position]
        nPositions = 1
        
    if time == '*':
        loopt = [t for t in range(ntime)]
    else:
        loopt = [time]
        ntime = 1
        
    if z == "*":
        loopz = [zi for zi in range(nslices)]
    else:
        loopz = [z]
        nslices = 1
    
    # Create the array
    dims = (ntime, nPositions, nslices, height, width, nchannels)
    output = np.empty(dims, dtype=np.dtype('uint16'))

    for t in range(ntime):
        for p in range(nPositions):
            for z in range(nslices):
                for c in range(nchannels):
                    name = mm1_name_pattern.format(pos=p,
                                                   time=t,
                                                   channel=channels[c],
                                                   z=z)
                    
                    try:
                        output[t,p,z,:,:,c] = imread(image_root_path+'/'+name)
                    except Exception as e:
                        print("Erreur de lecture de l'image %s" % name)
                        print(e)
                
    

    return output, meta

def find_paramater_in_MMmeta(metadata, parameter, multi=False):
    """
    Get a given parameter recursivly in metadata
    """

    if multi:
        output = []
    else:
        output = None    
        
    for i in metadata:
        if parameter in metadata[i]:
            if multi:
                output += [metadata[i][parameter]]
            else:
                output = metadata[i][parameter]
                break
            
    return output


def register_task(image, refimage, gaussian_size=0, up_factor=20, show_info=False):
    """
    Use phase correlation to compute the drift between two images.
    """

    if gaussian_size > 0:
        image = gaussian(image, gaussian_size)
        
    dxdyt, error, _ = phase_cross_correlation(refimage,
                                              image,
                                              upsample_factor=up_factor)

    if show_info:
        print('Displacement Error: %0.2e' % (error))
        
    return dxdyt


def register_task_sift(image, refimage, remove_cosmics=True, gaussian_size=0):
    """
    Use SIFT descriptor and RANSAC to obtain image shift
    """

    if remove_cosmics:
        el = disk(2)
        refimage = opening(refimage, el)
        image = opening(image, el)
        
    if gaussian_size > 0:
        image = gaussian(image, gaussian_size)
        refimage = gaussian(refimage, gaussian_size)
        
    dstimg = exposure.rescale_intensity(image)
    srcimg = exposure.rescale_intensity(refimage)
    
    # SIFT detector
    detect1 = SIFT()
    detect2 = SIFT()

    detect1.detect_and_extract(srcimg)
    detect2.detect_and_extract(dstimg)
    
    # Match detected points between the two images
    try:
        matched = match_descriptors(detect1.descriptors, detect2.descriptors, 
                                    max_ratio=0.6)
        src = detect1.keypoints[matched[:, 0]]
        dst = detect2.keypoints[matched[:, 1]]
    
        # Use ransac to find the transform matrix
        matrix, _ = ransac((src, dst), transform.EuclideanTransform, 
                            min_samples=10, 
                            residual_threshold=1)
          
        # print(matrix.translation)
        out = -matrix.translation
    except:
        out = (0, 0)
        
    return out


def register_images(imgs_stack, gaussian_size = 0,
                    register_filter=False, register_stack=True, ref_filter=1,
                    move_filter=0, show_info=False,
                    up_factor=20, drift_filter=None, 
                    remove_cosmics=True,
                    scale_correction=False,
                    drift_algo='cor'):
    """
    Register images to correct drift in time and between filter 
    
    Remove 5 pixel on border to remove PIXIS weird lines
    
    remove_cosmics: True,
        use an opening filter of disk size(2) to remove cosmics on images before registration
    
    register_filter: boolean,
        Do we need to register the other filter to the ref_filter
        
    register_stack: boolean,
        Do the registration of the whole stack
        
    scale_correction: boolean,
        Try to correct the scaling between filters (default False)
        
    drift_algo: str in ['cor', 'sift'],
        Define the algorithm to compute the drift between images.
        'cor': use crosscorrelation (the default)
        'sift': use SIFT point finding + RANSAC to find transform matrix
    """

    # Do we need to do scale correction
    if scale_correction:
        imgs_stack = correct_scaling(imgs_stack, ref_filter, move_filter)

    if drift_filter is None:
        filters = range(imgs_stack.shape[5])
    else:
        filters = [drift_filter]

    # A first drift correction in time
    if register_stack:
        print('Start stack registration !')
        for pos in range(imgs_stack.shape[1]):
            dxdy = []
            for img_filter in filters:
                if show_info:
                    print('stabilisation de la position pos %i' % pos)
                    print('stabilisation du filtre %i' % img_filter)

                imgref = imgs_stack[0, pos, 0, 5:-5, 5:-5, img_filter]

                if drift_algo == 'sift':
                    print('Use SIFT to align stack')
                    try:
                        dxdyt = Parallel(n_jobs=-1, backend='multiprocessing')(delayed(register_task_sift)(imgs_stack[t,pos,0, 5:-5, 5:-5, img_filter], 
                                                                               imgs_stack[t-1,pos,0, 5:-5, 5:-5, img_filter], remove_cosmics=remove_cosmics) for t in range(1, len(imgs_stack)))
                        dxdyt = np.cumsum(dxdyt, axis=0).tolist()

                    except:
                        print('Error in SIFT, failback to crosscorrelation registration')
                        drift_algo = 'cor'

                if drift_algo == 'cor':
                    print('Use crosscorrelation algorithm to align stack')
                    if remove_cosmics:
                        el = disk(2)
                        imgref = opening(imgref, el)

                    if gaussian_size > 0:
                        imgref = gaussian(imgref, gaussian_size)

                    # Non parrallel version
                    # dxdyt = [register_task(imgs_stack[t,pos,0,:,:,img_filter], imgref, up_factor=up_factor) for t in range(1, len(imgs_stack))]
                    # Parralel version
                    if remove_cosmics:
                        dxdyt = Parallel(n_jobs=-1, backend='multiprocessing')(delayed(register_task)(opening(imgs_stack[t,pos,0, 5:-5, 5:-5, img_filter], el), 
                                                                                imgref, up_factor=up_factor) for t in range(1, len(imgs_stack)))
                    else:
                        dxdyt = Parallel(n_jobs=-1, backend='multiprocessing')(delayed(register_task)(imgs_stack[t,pos,0, 5:-5, 5:-5, img_filter], 
                                                                                imgref, up_factor=up_factor) for t in range(1, len(imgs_stack)))

                dxdy += dxdyt

                if drift_filter is None:
                    for i, t in enumerate(range(1, len(imgs_stack))):
                        imgs_stack[t, pos, 0,:,:,img_filter] = ndi.shift(imgs_stack[t,pos,0,:,:,img_filter], dxdyt[i])

            if drift_filter is not None:
                for img_filter in range(imgs_stack.shape[5]):
                    if show_info:
                        print('stabilisation de la position pos %i' % pos)
                        print('stabilisation du filtre %i avec le déplacement filtre %i' % (img_filter, drift_filter))
                    for ti, t in enumerate(range(1, len(imgs_stack))):
                        imgs_stack[t, pos, 0,:,:,img_filter] = ndi.shift(imgs_stack[t,pos,0,:,:,img_filter], dxdy[ti])


    # A second drift correction between filters, only on the first time
    if register_filter:
        print('Register filters between two filters !')
        filter_ref = ref_filter
        filter_move = move_filter
        for pos in range(imgs_stack.shape[1]):
            # On prend la médiane sur l'ensemble des pas de temps
            imgref = imgs_stack[0, pos, 0, 50:-50, 50:-50, filter_ref]
            imgreg = imgs_stack[0, pos, 0, 50:-50, 50:-50, filter_move]

            if gaussian_size > 0:
                imgref = gaussian(imgref, gaussian_size)
                imgreg = gaussian(imgreg, gaussian_size)
                
            dxdyt, error, _ = phase_cross_correlation(imgref, imgreg,
                                                      upsample_factor=up_factor)
            
            if show_info:
                print('Error: %0.2e' % (error))
                
            for t in range(0, len(imgs_stack)):
                imgs_stack[t,pos,0,:,:,filter_move] = ndi.shift(imgs_stack[t,pos,0,:,:,filter_move], dxdyt)

    return imgs_stack
    

def bacteria_contour(bacteria_mask):
    """
    Extract bacteria contour from a bacteria mask obtain from segmentation
    
    Parameters
    ----------
    bacteria_mask: 2d array
        Mask which delineates each bacteria from the image
        
    Return
    ------
    list of x, y contours, shape (nbacteria, 2, len(x))
    """
    
    # Get labels of bacteria (exclude 0, it's the background)
    blabels = np.unique(bacteria_mask)[1:]
    contours = np.empty(len(blabels), dtype=object)
    
    for i, lb in enumerate(blabels):
        # Get the coordinate of bacteria contour
        contour = find_contours(bacteria_mask == lb, 0.5)[0]
        ye, xe = contour.T
        contours[i] = np.vstack((ye, xe))
        
    return contours


def enlarged_bacteria_contour(bacteria_mask, expand_distance=10):
    """
    Create bacteria contour from enlarged mask bacteria_mask: 2d array Mask
    which delineates each bacteria from the image

    expand_distance: int (optional)
        How far from bacteria contour the pixel should be sampled
        to extract the background.

    """

    # First expand bacteria mask
    expended = expand_labels(bacteria_mask, distance=expand_distance)

    # Get bacteria contour from expended mask
    bcontours = bacteria_contour(expended)

    return bcontours


def bacteria_bg(bacteria_contour, image, camera_bg = None):
    """
    Compute the bacground value around each bacteria defined by bacteria_mask.

    Parameters
    ----------
    bacteria_contour: list of 2d array
        Contour of bacteria to use

    image: 2d array (same size as bacteria_mask)
        Image used to extrack the pixel values for background.

    camera_bg: None or float,
        The value of camera background to use to set pixel lower boundary
        If None (the default), the background is computed automatically.
        
    Return
    ------

    Array of background (shape [number_bacteria])
    """


    # Get bacteria contour from expended mask
    bcontours = bacteria_contour

    # Get labels of bacteria (exclude 0, it's the background)
    # blabels = np.unique(bacteria_mask)[1:]
    bg_values = np.empty(len(bcontours))

    # Estimation of camera bg to fix a lower bound
    if camera_bg is None:
        camera_bg = np.percentile(image[image>0], (0.2,99.8))[0]

    for i in range(len(bcontours)):

        # Extract pixels of expended contour
        pixels_bg = ndi.map_coordinates(image,
                                        bcontours[i],
                                        mode='nearest',
                                        order=0)

        pixels_bg[pixels_bg <= camera_bg] = camera_bg

        # Smooth the profile
        try:
            pxe_smooth = ndi.median_filter(pixels_bg,
                                           size=len(pixels_bg)//3)
        except Exception as e:
            print('Error in smoothing use the row pixel_bg profile')
            print(pixels_bg)
            pxe_smooth = pixels_bg

        # Compute the bacground from this profile
        # Using a threashold based on max-min
        # threshbg = (pxe_smooth.max() - pxe_smooth.min()) * minmax_ratio + pxe_smooth.min()

        # Take the median of signal (with value only below the mean)
        threshbg = pxe_smooth.mean()

        bg = np.median(pxe_smooth[pxe_smooth <= threshbg])

        bg_values[i] = bg

    return bg_values

def bacteria_crosssection(bacteria_mask, bacteria_label, image, camera_bg=700):
    """Compute the crosssection pixel profile of a bacteria
    
    Parameters
    ----------
    bacteria_mask: 2d array
        bacteria mask for the image
        
    bacteria_label: int
        the selected bacteria (start at 1, not 0)
        
    image: 2d array
        image on wich to peform pixel crossection of the bacteria
    """
    
    # Trouver le background en temps
    properties = ('orientation', 'centroid', 
                  'minor_axis_length', 'major_axis_length')
    props = regionprops(bacteria_mask,
                        intensity_image=image)
    
    index = bacteria_label - 1
    assert props[index].label == bacteria_label
    
    yc, xc = props[index].centroid
    orientation = props[index].orientation
    x0 = xc - np.cos(orientation) * 1.5 * props[index].minor_axis_length
    x1 = xc + np.cos(orientation) * 1.5 * props[index].minor_axis_length
    y1 = yc - np.sin(orientation) * 1.5 * props[index].minor_axis_length
    y0 = yc + np.sin(orientation) * 1.5 * props[index].minor_axis_length
    
    xcross = np.linspace(x0, x1)
    ycross = np.linspace(y0, y1)
    pixels_cross = np.ma.masked_less_equal(ndi.map_coordinates(image,
                                                               np.vstack((ycross,xcross)),
                                                               mode='nearest'),
                                           camera_bg)
    
    return xcross, ycross, pixels_cross



def object_filter(label_image, min_size=0, max_size=100, min_eccentricity=1.0, 
                   max_eccentricity=0.7, px_per_micro=0):
    """
    Apply geometrical filters on detected objects

    exentricity (0=circle, 1=ellipse)

    If px_per_micro is zero the min_size and max_size are in pixel**2 otherwise
    they are in µm**2

    Return the new array of objects with filtered objects.
    """
    

    region_props = regionprops(label_image)
    
    # Create an array to save filtered objects
    good_regions = np.zeros_like(label_image, dtype=int)

    for prop in region_props:
        a = prop.area
        if px_per_micro > 0:
            a *= px_per_micro**2
            
        e = prop.eccentricity
        if a > min_size and a < max_size and e > min_eccentricity and e < max_eccentricity:
            good_regions += label_image == prop.label
        
    good_labels = label(good_regions > 0, background=0)
    
    return good_labels
    

def segment_bactos(image, image_band=1, denoise=True, 
                   min_size=0, max_size=100, max_eccentricity=1.0, 
                   min_eccentricity=0.7, pix_per_micron=0, tophat_size=None):
    """
    Bacteria segmentation using a threshold technic
    
    1 -> tophat_size = None -> estiamte size using round(1/sigma * 70)
    2 -> Apply white_top hat with a disk of size tophat_size
    3 -> denoise with denoise_wavelet
    4 -> apply yen threshold
    5 -> remove small/large objects and objects to close to img border
    6 -> label objects

    Parameters:
    -----------

    - image: 2d array (width, height, band),
        Image to precess

    - image_band:
        the image band to use

    - denoise (true/false):
        Use a denoise algorithm before threshold
    
    - min_size:
       min size of bacteria

    - max_size:
       max size of bacteria

    - min_eccentricity:
        min eccentricity of bacteria (0 = circle, 1 = ellipse)
    - max_eccentricity:
        max eccentricity of bacteria (0 = circle, 1 = ellipse)

    - pix_per_micron:
        Size of a pixel (if zero, all length are given in pixel),
                    
    - tophat_size (None):
        set the size of the element for the top hat filter, if None the size is
        estimated from the image noise asround(1/sigma * 100)
    """
    
    imt = image[:,:,image_band]

    if tophat_size is None:
        eps = estimate_sigma(imt)
        tophat_size = int(np.round(1/eps * 70))
        
    imt = white_tophat(imt, disk(tophat_size))
        
    if denoise:
        imt = denoise_wavelet(imt)

    thres = threshold_yen(imt)
    masks = imt > thres

    
    labels = object_filter(label(masks), min_size, max_size,
                        min_eccentricity, max_eccentricity, pix_per_micron)

    remove_border = clear_border(labels, buffer_size=2)
    
    labels = label(remove_border > 0, background=0)
    
    return labels



def segment_bactos_stardist(image, image_band=1, model=None, prob_thresh=None):
    """
    Use StarDist trained on TELEMOS bactos to segment bacteria on fluorescence images.

    Parameters:
    -----------

    - image, array (width, height, band):
        Image to process.

    - image_band, int:
        the band to use in the image array

    - model, object,
        The StarDist model used to process images

    - prob_thres, None or int (between 0 and 1):
        The probability threshold use in stardist prediction
    """
    
   
    # Select image 
    img = image[:,:,image_band]

    # Normalise image
    img = normalize(img)

    # Find bactos
    labels, details = model.predict_instances(img, prob_thresh=prob_thresh)
    
    return labels

    
def compute_label_filter_drift(labels, image_label, image_target, search_window=50):
    """
    Compute the drift between two filters using individual bacteria image search.

    Parameters:
    -----------
    - labels, 2d array:
        Label of bacteria

    - image_label, 2d array:
        Image used for the bacteria segmentation.

    - image_target, 2d array:
        Image on wich to compute the drift relative to image_label.

    - seach_window, int:
        Max distance to search the pattern on the target image

    Return a list of drift position for each bacteria label between the two filters.
    """

    regprops = regionprops(labels, intensity_image=image_label)
    
    alldx = np.empty(len(regprops))
    alldy = np.empty(len(regprops))
    
    for i, r in enumerate(regprops):
        aa = r.bbox
        cx, cy = r.centroid
    
        # Constrain search +-50pix around center of cell
        sdx = int(cx)-search_window
        sfx = int(cx)+search_window
        sdy = int(cy)-search_window
        sfy = int(cy)+search_window
    
        if sdx < 0:
            sdx = 0
        if sdy < 0:
            sdy = 0
        if sfx > image_label.shape[0]:
            sfx = image_label.shape[0]
        if sfy > image_label.shape[1]:
            sfy = image_label.shape[1]
        
        pattern = image_label[aa[0]:aa[2],aa[1]:aa[3]]
        result = match_template(image_target[sdx:sfx,sdy:sfy], template=pattern, pad_input=True)
        
        xn, yn = np.unravel_index(np.argmax(result), result.shape)
        xn += sdx
        yn += sdy
    
        alldx[i] = xn-cx
        alldy[i] = yn-cy
        
    return alldx, alldy


def register_filter_from_bactos(bactos, images, position=0, filter_ref=1, filter_target=0):
    """
    Register drift of target filter relative to ref filter based on pattern matching of bacteria

    - images, array:
        The full stack of images

    - bactos, array:
        the bacteria mask

    - postion, int:
        the position to process

    - filter_ref, int:
        the reference filter on which to extract bacteria pattern

    - filter_target, int :
        the filter to register based on bacteria pattern registration

    Return array (same dimensions as images) with registered filter_target
    """
    
    alldx, alldy = compute_label_filter_drift(bactos, 
                                              images[0, position, 0, :, :, filter_ref], 
                                              images[0, position, 0, :, :, filter_target])
    
    mdx = np.median(alldx)
    mdy = np.median(alldy)
    print('Median drift between filter % and filter %i from template matching: (%0.2f, %0.2f)' %(filter_ref, filter_target, mdx, mdy))
    
    for t in range(len(images)):
        images[t, position, 0, :, :, filter_target] = ndi.shift(images[t, position, 0, :, :, filter_target], 
                                                                (-mdx, -mdy))
        
        
    return images


def compute_bactos_properties(labels, images, position=0, pix_per_micron=1,
                              fluo_bg=700, channels = [0,1]):
    """
    Get data from bacteria mask

    Parameters:
    -----------
    - labels, array:
        The bacteria labels as 2d array

    - images, array:
        The image stack array

    - position, int:
        The position in the stack to process data

    - pix_per_micron, int:
        The scale

    - fluo_bg: int or float or 'auto',
        The value of pixel to offset fluorescence of bacteria. Could be
        manually set (usualy pixis 1024b camera has a value of 700 @ 30s).
        If 'auto' is given, the bacground is estimated using and expended
        contour of bacteria. The value is evaluated for each bacteria, each time
        step and each filter.

    - channels, list of int:
        The position of channels on which data extraction are done
    """

    regprops = regionprops(labels)
    bactos_area = [r.area * pix_per_micron**2 for r in regprops]
    bactos_perimeter = [r.perimeter * pix_per_micron for r in regprops]
    bactos_bbox = [r.bbox for r in regprops]
    bactos_labels = [r.label for r in regprops]

    if fluo_bg == 'auto':
        process_fluo = True
        el = disk(2)
        bacteria_contours = enlarged_bacteria_contour(labels)
    else:
        process_fluo = False

    bactos_mean_intensity = np.empty((len(images),len(np.unique(labels))-1, len(channels)))
    for t in range(len(images)):
        for chan in channels:
            regs = regionprops(labels, intensity_image=images[t, position, 0, :, :, chan])
            for i, r in enumerate(regs):
                bactos_mean_intensity[t,r.label-1, channels.index(chan)] = r.mean_intensity

            if process_fluo:
                # opening filter to remove cosmics!
                imgt = opening(images[t, position, 0, :, :, chan], el)
                fluo_bg = bacteria_bg(bacteria_contours, imgt)

            bactos_mean_intensity[t, :, channels.index(chan)] = bactos_mean_intensity[t, :, channels.index(chan)] - fluo_bg
                
    return bactos_area, bactos_perimeter, bactos_bbox, bactos_labels, bactos_mean_intensity
    

def extract_bactos(imgs_stack, mask_band=1, denoise=True,
                   bact_size=(0.5, 15), bact_eccent=(0, 1),
                   pix_per_micro=1, use_stardist=False, 
                   stardist_modelname = 'stardistBactosTELEMOS',
                   stardist_modeldir = '../stardist/models/',
                   background=700, channels=[0, 1], prob_thresh=None,
                   time_to_extract_bacteria=0):

    """
    Make mask on selected band for the time=0 and extract intensity
    and area information from masks

    Parameters:
    -----------

    - imgs_stack, array:
        The image stack array

    - mask_band, int:
        The stack band to extract bacteria

    - denoise, bool:
        Apply a denoising filter befor extracting bacteria

    - pix_per_micro, float:
        The physical size of images

    - use_stardist, bool:
        Use stardist to extract bacteria, otherwise use a threshold technic

    - stardist_modelname, str:
        The name of the trained model to use with stardist

    - stardist_modeldir, str:
        The path to the model file location

    - background: int/float or 'auto',
        Value of the background. If "auto" is given the bg is evaluated for each
        bacteria, each time, and each filter.

    - channels, list:
        list of channels to use in compute_bactos_properties

    - prob_thresh, None or a float value btwn 0 1:
        Set the probability threshold for stadist predict (use None to use the best one from the training)
        
    - time_to_extract_bacteria, int:
        The image to use in the time serie to extract the bacteria, the default is the first one so 0.
    """
    
    data = {}

    # Load stardist model if needed
    if use_stardist:
        # Load the trained model 
        conf = None
        stardist_model = StarDist2D(conf, name=stardist_modelname, basedir=stardist_modeldir)
    
    for p in range(imgs_stack.shape[1]):
        print('position %i' %p)
        tmpd = {}
        
        if use_stardist:
            labels = segment_bactos_stardist(imgs_stack[time_to_extract_bacteria,p,0], image_band=mask_band,
                                             model=stardist_model, prob_thresh=prob_thresh)
        else:
            labels = segment_bactos(imgs_stack[time_to_extract_bacteria,p,0], image_band=mask_band, denoise=denoise,
                                    min_size=bact_size[0], max_size=bact_size[1], min_eccentricity=bact_eccent[0],
                                    max_eccentricity=bact_eccent[1], pix_per_micron=pix_per_micro)
            
        print('Trouvé %i bactéries' % (np.unique(labels).max()-1))
        bare, bper, bbox, blab, bint = compute_bactos_properties(labels,
                                                                 imgs_stack,
                                                                 position=p,
                                                                 pix_per_micron=pix_per_micro,
                                                                 fluo_bg=background,
                                                                 channels=channels)
    
        for name, d in zip(('area', 'perim', 'labels', 'bbox', 'intens'), (bare, bper, blab, bbox, bint)):
            tmpd[name] = d
        
        tmpd['bmask'] = labels
        data['POS%i' % p] = tmpd.copy()

    return data


def update_bactos_data(bactos_data, imgs_stack, pix_per_micro=1, background=700,
                       channels=[0, 1]):
    """
    Update data from bacteria already segmented, usefull if you change image.

    Parameters:
    -----------
    - bactos_data, dict:
        The data extracted from bacteria mask

    - imgs_stack, array:
        The image stack array

    - pix_per_micro:
        The physical size of images

    - bactos_data, dict:
        Data for bacteria obtainer from "extract_bactos" function

    - background, int/float or 'auto':
        Value to remove to intensities of bacteria. If set to 'auto' background
        is estimated around each bacteria, for each time, each filter.

    - channels, list of int:
        The channels on wich we want to compute bactos_data
    """
    data = bactos_data.copy()

    for p in range(imgs_stack.shape[1]):
        print('process position %i' % p)
        tmpd = {}
        labels = data['POS%i' % p]['bmask']

        bare, bper, bbox, blab, bint = compute_bactos_properties(labels,
                                                                 imgs_stack,
                                                                 position=p,
                                                                 pix_per_micron=pix_per_micro,
                                                                 fluo_bg=background,
                                                                 channels=channels)

        for name, d in zip(('area', 'perim', 'labels', 'bbox', 'intens'),
                           (bare, bper, blab, bbox, bint)):
            tmpd[name] = d

        data['POS%i' % p].update(tmpd)

    return data


# Set the compression algorithm for hdf data
COMPRESSION = "gzip"

def save_to_h5(output_file, imgs_stack, bactos_data, img_metadata, gp_name,
               pix_per_micro, ctrl_filter=1, drug_filter=0):

    """
    Export data to hdf file.

    Parameters:
    -----------

    - output_file, str:
        The path and name of the output data file

    - imgs_stacl, array:
        The image stack

    - bactos_data, dict:
        The dictionnary of extracted bactos data

    - img_metadata, dict:
        The metadata of image stack extracted from MicroManager

    - gp_name, str:
        The name of the experiment to use to store those data in the hdf file

    - pix_per_micro, int:
        The physical size of images

    - ctrl_filter, int:
        The position of the Triptophane filter in the image stack

    - drug_filter, int:
        The position of the drug filter in the image stack
    """

    with h5py.File(output_file, 'a') as f:
        # Creation d'un groupe avec le meme nom que le fichier (soit le prefix stoquer dans les metadata de MM)
        datadir = gp_name
        # Si le groupe existe on le supprime
        if datadir in f:
            print('Data %s already exist I replace them with this one' % datadir)
            del f[datadir]
        else:
            print('Save data to group %s' %datadir)
            
        f.create_group(datadir)
        gp = f[datadir]
        
        # Creation des attribus
        gp.attrs['PixelSizeUm'] = pix_per_micro
        gp.attrs['dateprocessing'] = str(datetime.datetime.now())
        gp.attrs['channels_names'] = '; '.join(img_metadata['Summary']['ChNames'])
        gp.attrs['image_shape'] = '[time, position, z, row, col, channels]'
        gp.attrs['data_path'] = os.path.abspath(datadir)
        gp.attrs['ctrl_filter_pos'] = ctrl_filter
        gp.attrs['drug_filter_pos'] = drug_filter
        gp.create_dataset('images', data=imgs_stack, compression=COMPRESSION)

        if drug_filter < ctrl_filter:
            ch0 = 0
            ch1 = 1
        else:
            ch0 = 1
            ch1 = 0

        # Sauvegarde des données des bactos pour chaque positions
        for p in range(imgs_stack.shape[1]):
            gppos = gp.create_group('POS%i' % p)
            gppos.create_dataset('bacteria_mask', data=bactos_data['POS%i'%p]['bmask'], compression=COMPRESSION)
            gppos.create_dataset('ch0_bacteria_intensities', data=bactos_data['POS%i'%p]['intens'][:,:,ch0])
            gppos.create_dataset('ch1_bacteria_intensities', data=bactos_data['POS%i'%p]['intens'][:,:,ch1])
            gppos.create_dataset('bacteria_area', data=bactos_data['POS%i'%p]['area'])
            gppos.create_dataset('bacteria_perimeter', data=bactos_data['POS%i'%p]['perim'])
            gppos.create_dataset('bacteria_bbox', data=bactos_data['POS%i'%p]['bbox'])
            gppos.create_dataset('bactos_labels', data=bactos_data['POS%i'%p]['labels'])


    print('Data saved in %s' % output_file)

    
def run_processing(data_list, nom_sortie, MM_version=2, background='auto',
                   scale_correction=False, drug_filter=0, ctrl_filter=1,
                   stardist_path = None, bact_size=(0.5, 15), prob_thresh=None, 
                   time_to_extract_bacteria=0, build_bacteria_figures=True, 
                   bacteria_extract_mask='TRP', drift_algo='cor', register_filter=True, 
                   register_stack=True, register_mask=True):
    """
    Loop over the given folder name to extract data and export them inside one hdf file (containing all data from each folder)


    Parameters:
    -----------

    - data_list, list of str:
        MM root path to the data to process ['/my/path/data1/MMfolder/', '/my/path/data2/MMfolder/']

    - nom_sortie, str:
        Nom du fichier hdf5 sur lequel enregistrer les données

    - MM_version, 1 or 2:
        The file format of micromanager

    - background, int/float or 'auto'
        The value of bacground to be substracted to bacterie (default "auto", means get background around the bacteria!) 
        if a numeriacal value is given this value is used as background

    - scale_correction, boolean (default: False):
        Correct for scaling aberation due to chromatic aberation on ACAMAS

    - bact_size, tuple (min, max):
        The min size and max size of bacteria

    - prob_thresh, None or a float value bitween (0, 1)
        The probability threshold used by stardist predict (if None, use the best one from the training dataset)

    - time_to_extract_bacteria, int:
        The image to use in the time serie to extract the bacteria, the default is the first one so 0.
        
    - build_bacteria_figures, bool:
        Add a figure for each bacteria to the hdf5 file in an svg format. This figure contains a summary for each bacteria.
        
    - bacteria_extract_mask, str in ['TRP' or 'DRUG']:
    	Give the mask to extract the mask of bacteria. The default 'TRP' is on the control filter 
    	(the one of the tryptophan). If 'DRUG' is given the position of drug_filter is used 
    	
    - drift_algo, str in ['cor', 'sift']:
        Define the algorithm to compute the drift between images.
        - 'cor': use crosscorrelation (the default)
        - 'sift': use SIFT point finding + RANSAC to find transform matrix
        
    - register_filter, bool:
        Register the drug_filter on the ctrl_filter (default True)
        
    - register_stack, bool:
        Register the image stack using *drift_algo*, (default True). If false the 
        raw stack is used.
        
    - register_mask, bool:
        Register the two filter based on bacteria mask (default True)
        
    Example:
    --------

    expes = ['./my_path/to/wt_ctrl_condition/',
            './my_path/to/wt_and_drug/']

    out_file = 'WT_drug.h5'

    run_processing(expes, out_file)
    """
    
    #for i in track(range(len(data_list)), description="Processing data..."):
    for i in range(len(data_list)):
        c = data_list[i]
        datap = c
        last_dir = os.path.normpath(c).split(os.path.sep)[-1]
        print('Process %s, use name %s' % (c, last_dir))
        if MM_version == 1:
            imgs, metas = load_MM1_images(datap, channel='*', position='*', time='*', z=0)
            pix_per_micro = float(find_paramater_in_MMmeta(metas, 'PixelSize_um'))
        else:
            imgs, metas = load_MM2_images(datap, channel='*', position='*', time='*', z=0)
            pix_per_micro = float(find_paramater_in_MMmeta(metas, 'PixelSizeUm'))
            
        assert pix_per_micro is not None, 'pixel size should be defined'

        print(f"Filtre du contrôle: {metas['Summary']['ChNames'][ctrl_filter]}")
        print(f"Filtre de la drogue: {metas['Summary']['ChNames'][drug_filter]}")

        # Find stardist model
        test_paths = ['../python/stardist/models', './python/stardist/models',
                      '../stardist/models', './stardist/models']
        if stardist_path is not None:
            test_paths += [stardist_path]

        for p in test_paths:
            if os.path.exists(os.path.abspath(p)):
                startdist_path = p

        print(f'Stardist folder: {stardist_path}')

        
        try:
            date = dateutil.parser.parse(metas['Summary']['StartTime'])
        except:
	        date = dateutil.parser.parse(metas['Summary']['Time'])

        last_dir = '%s-%s' % (date.strftime('%d%m%Y'), last_dir)
        print('save with name %s' % last_dir)

    
        print('stabilisation')
        imgs = register_images(imgs, gaussian_size=0, move_filter=drug_filter,
                               ref_filter=ctrl_filter, register_filter=register_filter,
                               register_stack=register_stack, show_info=False, up_factor=10, drift_filter=0,
                               scale_correction=scale_correction, 
                               drift_algo=drift_algo)
    
        # Extract bacteria
        data = {}
        mask_band = ctrl_filter
        filter_ref = ctrl_filter
        filter_target = drug_filter
        if bacteria_extract_mask == 'DRUG':
            mask_band = drug_filter
            filter_ref = drug_filter
            filter_target = ctrl_filter
            
        bdata = extract_bactos(imgs, bact_size=bact_size, pix_per_micro=pix_per_micro,
                               use_stardist=True, stardist_modeldir=stardist_path,
                               mask_band=mask_band,
                               channels = sorted([drug_filter, ctrl_filter]),
                               prob_thresh=prob_thresh, 
                               time_to_extract_bacteria=time_to_extract_bacteria)
    
        # Estimate position between two filters using bacteria pattern matching
        if register_mask:
            if ctrl_filter != drug_filter:
                for pos in range(imgs.shape[1]):
                    print('Resgister filter1 and filter0 for pos %i' % pos)
                    imgs = register_filter_from_bactos(bdata['POS%i'%pos]['bmask'], imgs, position=pos,
                                                       filter_ref=filter_ref, filter_target=filter_target)

                # Update intensity data on re-registred images
                bdata = update_bactos_data(bdata, imgs, pix_per_micro, background=background,
                                           channels = sorted([drug_filter, ctrl_filter]))
        
        if len(bdata['POS0']['labels']) + len(bdata['POS1']['labels']) > 0:
            # Sauvegarde
            save_to_h5(nom_sortie, imgs, bdata, metas, last_dir, pix_per_micro,
                       drug_filter=drug_filter, ctrl_filter=ctrl_filter)
                       
            if build_bacteria_figures:
                for pos in range(imgs.shape[1]):
                    save_bacterias_svg_figures(nom_sortie, last_dir, pos)
        else:
            print('No bacteria found, no need to save the data')


def find_scaling(image_ref, image_scaled, radius=512):
    """
    Find scaling difference between two images using log-polar transform
    https://scikit-image.org/docs/dev/auto_examples/registration/plot_register_rotation.html#recover-rotation-and-scaling-differences-with-log-polar-transform

    Parameters:
    -----------

    - image_ref, array:
        the reference image

    - image_scaled, array:
        the image on which to adjust the scale

    - radius, int:
        The radius parameters used in logpolar transform
    """

    im1p = transform.warp_polar(image_ref, radius=radius,
                                scaling='log')
    im2p = transform.warp_polar(image_scaled, radius=radius,
                                scaling='log')

    # setting `upsample_factor` can increase precision
    shifts, error, phasediff = phase_cross_correlation(im1p, im2p,
                                                       upsample_factor=20)
    shiftr, shiftc = shifts[:2]

    # Calculate scale factor from translation
    klog = radius / np.log(radius)
    shift_scale = 1 / (np.exp(shiftc / klog))

    return shift_scale, im1p, im2p


def register_stack_for_scaling(img_stack, moving_image, scaling):
    '''
    Apply the scaling transformation on the image stack on all time steps

    Parameters:
    -----------

    - img_stack, array:
        The image stack

    - moving_image, int:
        the position of the moving (the image to rescale) in the stack

    - scaling, float:
        the scaling value for the image
    '''

    # Transform matrix
    nr, nc = img_stack[0,0,0,:,:,0].shape
    tform = transform.SimilarityTransform(scale=scaling, rotation=0,
                                          translation=((nr/2-nr/2*scaling),
                                                       (nc/2-nc/2*scaling)) # scale relative to the center
                                          )
    for t in range(img_stack.shape[0]):
        for pos in range(img_stack.shape[1]):
            img_stack[t,pos,0,:,:,moving_image] = transform.warp(img_stack[t,pos,0,:,:,moving_image], tform,
                                                                 preserve_range=True)

    return img_stack


def correct_scaling(img_stack, ref_image, moving_image):
    """
    Estimate and correct the stack from scaling aberation between two filters
    for the whole time steps of the stack.

    - img_stack, array:
        The stack of images

    - ref_image, int:
        The position of the reference image in the stack

    - moving_image, int:
        The position of the image to scale relativily to the reference one.
    """

    # Estimate the scaling on the first time of the stack
    s = []
    for pos in range(img_stack.shape[1]):
        im1 = img_as_float(img_stack[0,pos,0,:,:,ref_image])
        im2 = img_as_float(img_stack[0,pos,0,:,:,moving_image])

        # Remove cosmics
        el = disk(2)
        im1 = opening(im1, el)
        im2 = opening(im2, el)

        scaling, _, _ = find_scaling(im1, im2, int(im1.shape[0]/2))
        s += [scaling]
    # Take the mean of the scaling on the two positions
    scaling = np.mean(s)

    img_stack = register_stack_for_scaling(img_stack, moving_image, scaling)

    return img_stack
    
    
def get_bacteria_bbox(mask, bacteria_id, region_props=None, area_expend=0.5):
    """
    Return the coordinate of the bounding box of bacteria
    
    Paremeters:
    -----------
    
    - mask, 2D array:
        The mask of bacteria
        
    - bacteria_id, int:
        The bacteria that we want on the mask
        
    - region_props, None or scikit image regionprops object:
        The region props object for the mask could be passed to the function. 
        If its None, the region_props is evaluated using scikit image 
        
    return xmin, xmax, ymin, ymax
    """
    
    if region_props is None:
        region_props = regionprops(mask)
        
    r = region_props[bacteria_id]
    
    # Get crops around a bacteria
    bxmin, bymin, bxmax, bymax = r.bbox
    bheight = bxmax - bxmin
    bwidth = bymax - bymin
    bfcy, bfcx = r.centroid
    offset = area_expend
    maxsize = max((bwidth, bheight))
    new_size = maxsize + maxsize*offset
    
    cropxmin = int(bfcx - new_size/2)
    cropxmax = int(bfcx + new_size/2)
    cropymin = int(bfcy - new_size/2)
    cropymax = int(bfcy + new_size/2)
    
    if cropxmin < 0:
        cropxmin = 0
    if cropymin < 0:
        cropymin = 0
        
    if cropxmax > mask.shape[0]:
        cropxmax = mask.shape[0]
    if cropymax > mask.shape[1]:
        cropymax = mask.shape[1]
        
    return cropxmin, cropxmax, cropymin, cropymax


def bacteria_image_wall(imgs, pos, crop, channels=None, percentil=(1, 99)):
    """
    Create a unique image with all channels and all time of the bacteria
    
    Parameters:
    -----------
    
    - imgs, np array:
        array of images in the format [time, pos, z, y, x, channel]
        
    - pos, int:
        the position to use on the imgs stack
    
    - crop, list of int of size 4:
        the spatial limit of the stack defined as [xmin, xmax, ymin, ymax]
     
    -channels, None or list of channel position:
        Position of channel to stack vertically in the image_wall. If None all channel will be used.
           
    - percentil, tuple of size 2:
        the min percentil and max percentil to crop the intensities of imgs
        for autoscaling.
        
    return array
    """
    
    cropxmin, cropxmax, cropymin, cropymax = crop
    
    if channels is None:
        channels = list(range(imgs.shape[5]))
        
    imghstack = np.hstack(imgs[:, pos, 0, cropymin:cropymax, cropxmin:cropxmax, channels[0]])
    ps, pe = np.percentile(imghstack, percentil)
    imgwall = exposure.rescale_intensity(imghstack, in_range=(ps, pe))
    
    if len(channels) > 1:
        for ch in channels[1:]:
            imghstack = np.hstack(imgs[:,pos,0, cropymin:cropymax, cropxmin:cropxmax, ch])
            ps, pe = np.percentile(imghstack, percentil)
            imgwall = np.vstack((imgwall, exposure.rescale_intensity(imghstack, in_range=(ps, pe))))

    return imgwall
    
    
def bacteria_summary_graph(bacteria_ids, h5data, experiment, position, 
                           bacteria_offset=0.5, export_as_svg=True):
    """
    Build a graphic thats shows pictures time series of a given bacteria
    and the evolution of it's fluorescence.
    
    Parameters:
    -----------
    
    - bacteria_ids, (int or float) or 'all' or a list of int or float:
        The bacteria id of list of bacteria ids to use. If 'all' take the unique 
        value of the mask to get all bacterias.
        
    - h5data, str:
        The path to the data in hdf format
        
    - experiment, str:
        The name of the experiment. Must be contained in the h5data
        
    - position, int:
        The position, if multi stage position has been used on the microscope, to use
        
    - bacteria_offset, float:
        The size offset (in percent) to add around the bacteria bounding box
        
    - export_as_svg, bool:
        Export the figure as svg format if True (default). If False return a list of 
        matplotlib figure objects.
        
    return a list of figures
    """
    
    
    if isinstance(bacteria_ids, (int, float)):
        bacteria_ids = [bacteria_ids]
    
    h5data = Path(h5data)
    
    # Load the data
    pos = position
    exp = experiment
    imgs = load_image(h5data, exp)
    masks = load_mask(h5data, exp, pos)
    bactosct = bacteria_contour(masks)
    Ib_chan0 = load_intensity_profile(h5data, exp, 0, pos)
    Ib_chan1 = load_intensity_profile(h5data, exp, 1, pos)
    
    if isinstance(bacteria_ids, str) and bacteria_ids == 'all':
        bacteria_ids = np.unique(masks)[1:]-1
        
    # Compute the properties of bacteria masks
    rprops = regionprops(masks)

    # This list will contain all figures (one for each bacteria)
    figures = []
    
    for bid in bacteria_ids:
        r = rprops[bid]
        
        # Get crops around a bacteria
        cropxmin, cropxmax, cropymin, cropymax = get_bacteria_bbox(masks, bid, rprops, bacteria_offset)
        # Compute the new width and height for this bacteria
        nwidth = cropxmax - cropxmin
        nheight = cropymax - cropymin
        
        # Get the centroid of bacteria
        bfcy, bfcx = r.centroid
        
        # Start to build the figure
        fig = plt.figure(figsize=(13, 1.5))
        plt.title(f'Exp: {exp}, Pos: {pos}, bacteria-{bid+1} (in file {h5data.name})')
        G = plt.GridSpec(1, 9)
        
        # The main image to locate the bacteria on the FOV
        plt.subplot(G[0, 0])
        p2, p98 = np.percentile(imgs[0,pos,0, ::2, ::2, 0], (2, 98))
        plt.imshow(exposure.rescale_intensity(imgs[0,pos,0, ::2, ::2, 0], in_range=(p2, p98)), 'gray')
        plt.plot(bfcx/2, bfcy/2, 'ro', fillstyle='none')
        plt.axis('off')
        plt.text(-8, imgs.shape[4]/4, f'Bacteria-{bid+1}', rotation=90, ha='right', va='center')
        plt.text(imgs.shape[4]/4, 0, f'POS - {pos}', ha='center', va='bottom')
        
        # Create the timewall of all images of this bacteria through time and filters
        plt.subplot(G[0, 1:4])
        imgwall = bacteria_image_wall(imgs, pos, (cropxmin, cropxmax, cropymin, cropymax))
        plt.imshow(imgwall, 'gray')
        
        # Add channel names
        for ch in range(imgs.shape[5]):
            plt.text(-5, nheight/2+ch*nheight, f'C-{ch}', ha='right', va='center')
    
        # Plot the contour of bacteria on first row of the timewall and for all time steps
        for j in range(len(imgs)):
            plt.plot((bactosct[bid][1] + j*nwidth)-cropxmin, bactosct[bid][0]-cropymin, 'C3--')
        
        plt.axis('off')
    
        # Plot the mean intensity of each filters inside the segmented bacteria
        plt.subplot(G[0, 4:])
        plt.plot(Ib_chan1[:, bid], 'C3o-')
        ax = plt.gca()
        ax.spines["right"].set_visible(False)
        ax.spines["top"].set_visible(False)
        ax.set_ylabel('I-C-0', color='C3')
        ax.tick_params(axis='y', labelcolor='C3')
    
        ax2 = ax.twinx()
        ax2.plot(Ib_chan0[:, bid], 'C0o-')
        ax2.spines["left"].set_visible(False)
        ax2.spines["top"].set_visible(False)
        ax2.tick_params(axis='y', labelcolor='C0')
        ax2.set_ylabel('I-C-1', color='C0')
    
    
        plt.tight_layout(pad=0.9)
        
        if export_as_svg:
            fsvg = io.BytesIO()
            plt.savefig(fsvg, format='svg')
            figures += [fsvg.getvalue().strip().replace(b'\n', b'')]
            plt.close(fig)
            del fig
        else:
            figures += [fig]
        
    return figures


def save_bacterias_svg_figures(h5file, exp, pos):
    """
    Save figures in svg format for each bacteria, the figure shows a closed view of the 
    bacteria it's location and the signal inside the mask with time.
    """
    
    # create all the figures 
    print('Create svg figures in Parralel')
    nbactos = np.unique(load_mask(h5file, exp, pos))[1:]-1
    figs = Parallel(n_jobs=-1, backend='multiprocessing')(delayed(bacteria_summary_graph)(int(b), h5file, exp, pos) for b in nbactos)
    figs = [f[0] for f in figs]
    
    # Not in parralel we can use the following function
    # figs = bacteria_summary_graph('all', h5file, exp, pos)
    
    print(f'Save svg figures to {exp}/{pos}/bacteria_svg_figures in {h5file}')
    with h5py.File(h5file, 'a') as f:
        pos = 'POS%i' % pos
        if 'bacteria_svg_figures' in f[exp][pos]:
            del f[exp][pos]['bacteria_svg_figures']
            
        f[exp][pos].create_dataset('bacteria_svg_figures', data=figs, compression="gzip")
