#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 20 16:06:58 2024

Evolution of bactos_functions.py to track bacteria

@author: chauveth
"""
from bactos_functions import StarDist2D, segment_bactos_stardist, bacteria_bg, enlarged_bacteria_contour
import numpy as np

# For the tracking of bacteria
from skimage import measure
from stracking.linkers import SPLinker, EuclideanCost
from stracking.detectors import SSegDetector

# Extract information from bacteria masks
from skimage.measure import regionprops_table
from skimage.morphology import disk, opening
import matplotlib.animation as animation
import matplotlib.pyplot as plt

# To merge the data and make a nice table
import pandas as pd

# Find a mean bacteria over time
from scipy.ndimage import rotate, center_of_mass

# For darkwhite correction
import tifffile
from skimage import morphology
from skimage import filters
from skimage import img_as_float, img_as_uint

## New function to do so
def get_time_from_metadata(metadata: dict, position: int = 0, channel: int = 0, z: int = 0):
    """
    Return the time from the metadata for the given position, channel, and z
    """
    
    t = 0
    nt = metadata['Summary']['Frames']
    
    allt = np.empty(nt)
    for t in range(nt):
        meta_data_key = f'Metadata-Pos{position}/img_channel{channel:03d}_position{position:03d}_time{t:09d}_z{z:03d}.tif'
        
        if meta_data_key not in metadata:
            raise KeyError(f'No {meta_data_key} in metadata')
            
        allt[t] = (metadata[meta_data_key]['ElapsedTime-ms'] / 1000) / 60
        
    return allt


def get_time_for_all_data(data: np.array, metadata: dict):
    """
    Return an array with the a size compatible with the one of image data (contained in data array)
    """
    
    s = data.shape
    
    allt = np.empty((s[0], s[1], s[2], s[5]))
    for pos in range(s[1]):
        for z in range(s[2]):
            for chan in range(s[5]):
                allt[:, pos, z, chan] = get_time_from_metadata(metadata, pos, chan, z)
                
                
    return allt


def create_bacteria_masks_time(image_stack, channel, pix_size=0.14, 
                               stardist_modeldir = './stardist/models/', 
                               stardist_modelname = 'stardistBactosTELEMOS',
                               prob_thresh=None):
    """
    Find bacteria for each time and each position of the image stack using stardist
    """
    
    stardist_model = StarDist2D(None, name=stardist_modelname, basedir=stardist_modeldir)
        
    labels = np.zeros([image_stack.shape[0], image_stack.shape[1], image_stack.shape[-3], image_stack.shape[-2]], dtype=int)
    for t in range(labels.shape[0]):
        print(f'Process time {t} with {labels.shape[1]} positions')
        for p in range(labels.shape[1]):
            labels[t, p, ...] = segment_bactos_stardist(image_stack[t,p, 0], image_band=channel,
                                                     model=stardist_model, prob_thresh=prob_thresh)
        
    return labels


def track_bacteria(segmented_stack, max_cost=3000, gap=3, min_track_length=10, min_track_pos_dist=10, reverse=True):
    """
    Function to track bacteria from a stack of segmented masks 
    
    Source:
    - https://sylvainprigent.github.io/stracking/stardist_stracking.html
    - https://hal.science/hal-03688217v1/file/STracking_hal.pdf
    """

        
    # Setup the tracker parameters
    euclidean_cost = EuclideanCost(max_cost=max_cost)
    my_tracker = SPLinker(cost=euclidean_cost, gap=gap, min_track_length=min_track_length)
    
    # Create the particles from masks 
    # We track the bacteria from the end to the beginning, since we are 
    # more interested in bacteria that are still there at the end
    sdetector = SSegDetector(is_mask=False)
    if reverse:
        particles = sdetector.run(segmented_stack[::-1])
    else:
        particles = sdetector.run(segmented_stack)
    
    Ntimes = segmented_stack.shape[0] 
    
    # Run the tracking of particles
    print('start tracking')
    tracks = my_tracker.run(particles)

    # Create a dataframe from the tracks founded by the tracker
    ptracks = pd.DataFrame(tracks.data, columns=['traj','frame','y','x'])

    if reverse:
        print('Warning if reverse, start detection from the end, could lead to buggy thing if black images at the end of the sequence')
        ptracks.frame = (Ntimes - 1) - (ptracks.frame)
    
    print('Create new masks with corrected bacteria labels')
    # Now we create new masks based on track to assign the same label for bateria over time
    new_bmasks = np.zeros_like(segmented_stack)

    for t in range(segmented_stack.shape[0]):
        for r in measure.regionprops(segmented_stack[t]):
            y,x = r.centroid
    
            trajs = ptracks[ptracks.frame == t]

            dist = np.array(np.sqrt((trajs.y - y)**2 + (trajs.x - x)**2))
    
            if len(dist) > 0:
                mindist_pos = np.argmin(dist)
                
                if dist[mindist_pos] < min_track_pos_dist:
                    new_bmasks[t, r.coords[:,0], r.coords[:,1]] = trajs.iloc[mindist_pos].traj + 1
          
    return new_bmasks, ptracks


# propagate bacteria on the first frame (where their is no fluorescence)
def inter_nobacteria_frames(input_mask, use_max=False, use_time=False):
    """
    Find frame where no bacteria are present and copy the bacteria from 
    the closest frame
    """
    
    nbacts = np.array([sum(np.unique(input_mask[t])) for t in range(len(input_mask))])
    nobacts = np.argwhere(nbacts == 0)
    isbacts = np.argwhere(nbacts > 0)
    if use_max:
        isbacts = np.argmax(nbacts)

    if use_time and use_time > 0:
        isbacts = int(use_time)
        nobacts = tuple(range(use_time))
        
    interp_bmasks = input_mask.copy()

    for izero in nobacts:

        dist_to_bact_indices = np.min(isbacts-nobacts[izero])
        icopy = nobacts[izero] + dist_to_bact_indices
        print(f"Copy bacteria from {icopy} to {izero}")
    
    
        interp_bmasks[izero] = interp_bmasks[icopy]
        
    return interp_bmasks


def get_bacteria_properties(bacteria_mask, images, metadata, position, channels = [0, 1]):
    """
    Extract properties from bacteria mask and return them as panda DataFrame
    """
    
    data = pd.DataFrame({})

    pos = position
    el = disk(2)

    # Compute the camera noise 
    bg_camera = []
    for c in channels:
        # get the min values over all time acquisitions
        d = images[:,0,0,...,c].min(0)
        try:
            bg_camera += [ np.percentile(d[d>0], (0.2,99.8))[0] ]
        except:
            bg_camera += [ 0 ]
    
    # Get the real time from metadata
    try:
        pictures_times = get_time_for_all_data(images, metadata)
    except Exception as e:
        print('Unable to extract time from metadata, use faketime instead')
        print(e)
        pictures_times = None
    
    for t in range(len(bacteria_mask)):
        rprops = regionprops_table(bacteria_mask[t], 
                                   properties=('label', 'bbox', 'area', 'perimeter', 'centroid', 'orientation'))
        
        # for intensities props:
        for c in channels:
            intprops = regionprops_table(bacteria_mask[t], intensity_image=images[t,pos,0,:,:,c], 
                                         properties=('intensity_mean', 'intensity_max', 'intensity_min'))
            
            rprops[f'intensity_mean-{c}'] = intprops['intensity_mean']
            rprops[f'intensity_max-{c}'] = intprops['intensity_max']
            rprops[f'intensity_min-{c}'] = intprops['intensity_min']
            
            # Compute the intensity of the background near each bacteria
            
            larger_bacteria_contours = enlarged_bacteria_contour(bacteria_mask[t])
            # opening filter to remove cosmics!
            imgt = opening(images[t, pos, 0, :, :, c], el)
            rprops[f'backgound_mean-{c}'] = bacteria_bg(larger_bacteria_contours, imgt, bg_camera[c])
            if pictures_times is not None:
                rprops[f'time-{c}'] = np.array([pictures_times[t,pos,0,c]] * len(rprops['label']))
            else:
                rprops[f'time-{c}'] = np.array([t] * len(rprops['label']))
        
        rprops['faketime'] = np.array([t] * len(rprops['label']))
        rprops['position'] = np.array([pos] * len(rprops['label']))
        data = pd.concat((data, pd.DataFrame(rprops)), ignore_index=True)

    return data


# Find a mean bacteria over time
def aligned_bacteria_mask(bacteria_data, bacteria_masks):
    """
    Average the shape of bacteria over time and 
    export the mean mask for earch bacteria.

    Parameters:
    -----------

    - bacteria_data, dask DataFrame:
        The properties of bacteria contours must cotains ('label', 'bbox', 'orientation')

    - bacteria_mask, array:
        The array with all bacteria mask and all times

    Return:
    -------

    An array with dimension (bacteria, t, 50, 50) with the vertically aligned bacteria masks.
    
    """


    bacteria_label = bacteria_data['label'].unique()
    Ntime = len(bacteria_masks)
    
    imgf = np.zeros((len(bacteria_label), Ntime, 50,50), dtype=int)

    for ib, b in enumerate(bacteria_label):
        b1 = bacteria_data[bacteria_data['label'] == b]
        
        for t in range(Ntime):
            b1t = b1[b1['faketime'] == t]

            if len(b1t) > 0:
                imgt = bacteria_masks[t, 
                                      int(b1t['bbox-0'].iloc[0]):int(b1t['bbox-2'].iloc[0]), 
                                      int(b1t['bbox-1'].iloc[0]):int(b1t['bbox-3'].iloc[0])]
        
                if imgt.max() > 0:
                    imgtr = rotate(imgt, np.rad2deg(-b1t['orientation'].iloc[0]), order=1)
                    imgtr[imgtr>=0.1] = 1
                    imgtr[imgtr<0.1] = 0
                    
                    ycenter, xcenter = center_of_mass(imgtr)
                    ycenter = int(ycenter)
                    xcenter = int(xcenter)
            
                    imgf[ib, t, 25-ycenter:imgtr.shape[0] + (25-ycenter), 25-xcenter:imgtr.shape[1] + (25-xcenter)] = imgtr


    return imgf


# Replace bacteria masks at each time by their mean value
def project_mean_bacteria_on_global_mask(initial_bacteria_mask, aligned_bacteria_mask, bacteria_data, mean_cut=0.7):
    """
    Take each individual aligned bactéria, compute their mean and project this mean 
    shape on the initial mask done on the dateset.

    Parameters:
    -----------

    - initial_bacteria_mask, array (dimension of microscope image with time):
        The mask donne at each time on initial images 
    - aligned_bacteria_mask, array dimension: (bacteria, time, 50, 50):
        The mask of aligned bacteria (see align_bacteria_mask function)
    - bacteria_data: pandas DataFrame:
        Contains bacteria properties extracted on initial_bacteria_mask
    - mean_cur: float between 0.1 and 1
        The value to make the new mask on the mean value (the default is 0.7)
    
    Return:
    -------

    An array with the same dimention of initial_bacteria_mask filled 
    with the mean shape of bacteria.
    """


    bacteria_label = bacteria_data['label'].unique()
    Ntime = len(initial_bacteria_mask)

    mean_mask = np.zeros_like(initial_bacteria_mask)

    # Compute the mean of bacteria aligned
    bmeans = aligned_bacteria_mask.mean(axis=1)
    # Norm the means by the maximum of each time
    bmeans = bmeans / bmeans.max(axis=(-1,-2))[:, None, None]

    # Make a binary image based on a threashold of 0.7
    bmeans[bmeans >= 0.7] = 1
    bmeans[bmeans<0.7] = 0

    print(bmeans.shape)
    # Loop over time
    for t in range(Ntime):
        # Loop over bacteria 
        for ib, b in enumerate(bacteria_label):
            b1 = bacteria_data[bacteria_data['label'] == b]
            b1t = b1[b1['faketime'] == t]
            # print(b,t)
            if len(b1t) > 0:
                b1t = b1t.iloc[0]
                
                # rotate back the mean bacteria to the orientation at the given time
                img_bmean_rotated = rotate(bmeans[ib], np.rad2deg(b1t['orientation']), order=1)
                img_bmean_rotated[img_bmean_rotated>=0.1] = b1t['label']
                img_bmean_rotated[img_bmean_rotated<0.1] = 0

                # Need to get back project the bacteria to the camera frame referential (x, y) translation
                data_bmean = pd.DataFrame(regionprops_table(img_bmean_rotated.astype(int), properties=('label', 'bbox', 'centroid')))

                dx = int(b1t['centroid-0'] - data_bmean['centroid-0'][0])
                dy = int(b1t['centroid-1'] - data_bmean['centroid-1'][0])

                # Manager borders
                startx = 0
                if dx < 0:
                    startx = int(0-dx)
                    dx = 0

                starty = 0
                if dy < 0:
                    starty = int(0-dy)
                    dy = 0
                    
                up_limx = dx + img_bmean_rotated.shape[0]
                if up_limx > mean_mask.shape[1]:
                    up_limx = mean_mask.shape[1]
                sizex = up_limx - dx
                
                up_limy = dy + img_bmean_rotated.shape[1]
                if up_limy > mean_mask.shape[2]:
                    up_limy = mean_mask.shape[2]
                sizey = up_limy - dy
                
                # Make a copy of alread present bacteria to restore them (as they could be overlayed by the zero of the new bacteria)
                already_set_bacteria = mean_mask[t].copy()
                mean_mask[t, dx:up_limx-startx, dy:up_limy-starty] = img_bmean_rotated[startx:sizex, starty:sizey]
                mean_mask[t, already_set_bacteria>0] = already_set_bacteria[already_set_bacteria>0]

    return mean_mask


def normalize_illumination_beam(white_image: np.array, dark_of_white_image: np.array = None,
                                limit_min_ratio: bool = True):
    """
    Normalize the illumination beam. This function will compute:
    Normed_illumintaion = (white_image - dark_of_white_image) / max(white_image - dark_of_white_image)
    
    
    If limit_min_ratio is True (default), compute the minRatio = white_image.min()/max(white_image - dark_of_white_image) 
    and replace Normed_illumintaion < minRatio with minRatio

    Parameters
    ----------
    white_image, array (or dask array):
        the array with white image stack (could be a z-stack)
        
    dark_of_white_image, array (or dask array):
        the array with the dark of white image (i.e. the camera noise for the 
        exposure time used to acquire white images)
        
    return 
    ------
    
    the normalized illumination beam as a float image
    """
    
    float_w = img_as_float(white_image) 
    float_dow = img_as_float(dark_of_white_image)

    diff_w = float_w - float_dow 
    max_w = diff_w.max()
    minRatio = float_w.min() / max_w

    corrected_w = diff_w / max_w
    
    if limit_min_ratio:
        corrected_w[corrected_w < minRatio] = minRatio
    
    return corrected_w


def correct_dark_white_one_filter(data: np.array, dark_image_path: str, white_image_path: str, dark_of_withe_path: str, roi: list):
    """
    Simple function to apply dark white correction to one filter
    """
    
    disk = morphology.disk(1)
    white = filters.gaussian(morphology.opening(tifffile.imread(white_image_path)[roi[0]:roi[0]+roi[2], roi[1]:roi[1]+roi[3]], disk), 10)
    darkofwhite = filters.gaussian(morphology.opening(tifffile.imread(dark_of_withe_path)[roi[0]:roi[0]+roi[2], roi[1]:roi[1]+roi[3]], disk), 10)
    dark = filters.gaussian(morphology.opening(tifffile.imread(dark_image_path), disk), 10)
    

    normed_white = normalize_illumination_beam(white, darkofwhite, limit_min_ratio=True)


    fluo_imgs = img_as_float(data) - img_as_float(dark)
    fluo_imgs[fluo_imgs<0] = 0
    
    return img_as_uint(fluo_imgs / normed_white)


def mask2contour(bacteria_mask, colors=None, add_label=False, axes=None):
    """
    Fonction to plot bacteria contour from a mask
    
    Parameters:
    -----------
    - colors, array,
        Contain colors for each bacteria generated from matplotlib colormap
        
        
    Exemple:
    --------
    
    # List the bacteria unique id for each time and a given position
    i_bactos = np.unique(bacteria_masks[:, select_pos])
    # Create the colormap
    C = plt.cm.turbo(i_bactos[1:]/(len(i_bactos[1:])))
    
    mask2contour(bacteria_masks[0, select_pos], C)
    """

    if colors is None:
        colors = plt.cm.turbo(np.unique(bacteria_mask[1:])/len(np.unique(bacteria_mask)[1:]))
        
    if axes is None:
        axes = plt.gca()
        
    for l in np.unique(bacteria_mask):
        if l != 0:
            
            axes.contour(bacteria_mask == l, levels=[0.5,], colors=[colors[l-1],])
            
            if add_label:
                
                # Find the center of the contour
                Y, X = np.where(bacteria_mask == l)
                Y = int(Y.mean())
                X = int(X.mean())
                axes.text(X, Y, l, color=colors[l-1])

                
def create_gif(data_path, images, bacteria_masks, ivis=1, ifluo=0, fluo_title=r'Fluo (DM405_480-550_Fluo)', vis_title='Visible'):
    """
    Function to create a gif animation for the tracking of bacteria
    """
    
    
    for select_pos in range(images.shape[1]):
        print(f'Export gif for position {select_pos}')
        
        # compute the lower and upper limit for the entire time sequence
        pmin, pmax = np.percentile(images[:, select_pos, 0, ..., ifluo], (0.2,99.8), axis=(1,2))
        lpmin_fluo = pmin.min()
        hpmax_fluo = pmax.max()
        pmin, pmax = np.percentile(images[:, select_pos, 0, ..., ivis], (0.2,99.8), axis=(1,2))
        lpmin_vis = pmin.min()
        hpmax_vis = pmax.max()

                
        # List all the bacteria 
        i_bactos = np.unique(bacteria_masks[:, select_pos])
        C = plt.cm.turbo(i_bactos[1:]/(len(i_bactos[1:])))

        # print(C)
        writer = animation.PillowWriter(fps=5,
                                        metadata=dict(artist='H. Chauvet'),
                                        bitrate=1800)

        fig = plt.figure(figsize=(10,5))
        gifname = data_path.parent / f"{data_path.name}-pos{select_pos}.gif"
        with writer.saving(fig, gifname, 100):
            for i in range(images.shape[0]):
                G = plt.GridSpec(ncols=2, nrows=1)
                axl = plt.subplot(G[0])
                axg = plt.subplot(G[1])
                plt.suptitle(f'{data_path.name} - position {select_pos}, time {i:02d} min')
                axl.set_title(fluo_title)
                axl.set_axis_off()
                axl.imshow(images[i, select_pos, 0,...,ifluo], 'gray', vmin=lpmin_fluo, vmax=hpmax_fluo)
                mask2contour(bacteria_masks[i, select_pos], C, True, axes=axl)
                axg.imshow(images[i, select_pos, 0,...,ivis], 'gray', vmin=lpmin_vis, vmax=hpmax_vis)
                mask2contour(bacteria_masks[i, select_pos], C, True, axes=axg)
                axg.set_title(vis_title)
                axg.set_axis_off()
        
                if i == 0:
                    plt.tight_layout()
            
                writer.grab_frame()
                fig.clear()

        del fig 
        del writer