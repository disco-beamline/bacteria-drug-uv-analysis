import numpy as np
import pandas as pd
from skimage.exposure import rescale_intensity
import skimage.util as skutil
from skimage.measure import find_contours
from pathlib import Path
import tifffile
import matplotlib.pyplot as plt


def get_bacteria_bbox(bounding_boxes, centroids, image_shape=(512, 512), area_expend=0.5):
    """
    Return the coordinate of the bounding box of bacteria

    Paremeters:
    -----------

    - mask, 2D array:
        The mask of bacteria

    - bacteria_id, int:
        The bacteria that we want on the mask

    - region_props, None or scikit image regionprops object:
        The region props object for the mask could be passed to the function.
        If its None, the region_props is evaluated using scikit image

    return xmin, xmax, ymin, ymax
    """

    # Get crops around a bacteria
    bxmin, bymin, bxmax, bymax = bounding_boxes
    bheight = bxmax - bxmin
    bwidth = bymax - bymin
    bfcy, bfcx = centroids
    offset = area_expend
    maxsize = max((bwidth, bheight))
    new_size = maxsize + maxsize*offset

    cropxmin = int(bfcx - new_size/2) + 1
    cropxmax = int(bfcx + new_size/2) + 1
    cropymin = int(bfcy - new_size/2) + 1
    cropymax = int(bfcy + new_size/2) + 1

    if cropxmin < 0:
        cropxmin = 0
    if cropymin < 0:
        cropymin = 0

    if cropxmax > image_shape[0]:
        cropxmax = image_shape[0]
    if cropymax > image_shape[1]:
        cropymax = image_shape[1]

    return cropxmin, cropxmax, cropymin, cropymax


def bacteria_stipe(image_stack, bacteria_data, max_time_pos=15, bacteria_masks=None):
    """
    Create a stipe of images for one bacterria

    Parameters:
    -----------

    - image_stack, array with images [t, Y, X]:
        The image tha will be used to create the stripe

    - bacteria_data, pandas DataFrame:
        The data extracted from the tracking of bacteria
        (give the data given by a groupby(['position', 'label']))

    - max_time_pos, int:
        The maximum of time point on the stripe (usualy 15), could be optain from
        len(bacteria['faketime'].unique())

    - bacteria_masks, none or same shape of the image_stack:
        The masks for the given bacteria

    return:
    -------

    An array with all time images stitched along the x axis.
    If bacteria_masks is not None, return also the strip for the mask.
    """

    # First compute the largest bounding box over the tracking
    bboxes = np.hstack((bacteria_data['bbox-0'].min(), bacteria_data['bbox-1'].min(),
                        bacteria_data['bbox-2'].max(), bacteria_data['bbox-3'].max()))


    im_shape = (image_stack.shape[-2], image_stack.shape[-1])


    # Compute for the time zero to get the image shape
    datat = bacteria_data.iloc[0]
    centroids = np.hstack((int(datat['centroid-0']), int(datat['centroid-1'])))
    xmin, xmax, ymin, ymax = get_bacteria_bbox(bboxes, centroids, image_shape=im_shape)

    image_stripes = np.zeros((max_time_pos, ymax-ymin, xmax-xmin))
    if bacteria_masks is not None:
        mask_stripes = np.zeros_like(image_stripes)

    # Loop overtime to get the centroid and crop the image
    for t in range(len(bacteria_data)):
        datat = bacteria_data.iloc[t]
        centroids = np.hstack((int(datat['centroid-0']), int(datat['centroid-1'])))
        xmin, xmax, ymin, ymax = get_bacteria_bbox(bboxes, centroids, image_shape=im_shape)

        tmp_img = image_stack[t, ymin:ymax, xmin:xmax]

        try:
            image_stripes[t, :tmp_img.shape[0], :tmp_img.shape[1]] = tmp_img
            if bacteria_masks is not None:
                mask_stripes[t, :tmp_img.shape[0], :tmp_img.shape[1]] = bacteria_masks[t, ymin:ymax, xmin:xmax]
        except:
            pass


    if bacteria_masks is not None:
        return np.hstack(image_stripes), np.hstack(mask_stripes)

    return np.hstack(image_stripes)


def plot_bacteria_summary(bacteria_data, vis_img, drug_img, trp_img, bmasks,
                          pos, label, expe_name='Test', max_time_pos=15):
    """
    Create a figure to get the overview of the accumulation for one bacteria at one position
    (vis,trp,drug,bmask) should have the dimension of [t, Y, Z]
    """


    plt.figure(figsize=(10,2.5*3))
    G = plt.GridSpec(3,3)

    # Plot a general view to locate the bacteria
    plt.subplot(G[0,0])
    plt.imshow(vis_img[0], 'gray')
    plt.plot(bacteria_data.iloc[0]['centroid-1'], bacteria_data.iloc[0]['centroid-0'], 'C3o',
             fillstyle='none')
    plt.axis('off')

    # Plot the displacement of the centroid
    plt.subplot(G[0,1])
    dy = bacteria_data['centroid-0'] - bacteria_data['centroid-0'].iloc[0]
    dx = bacteria_data['centroid-1'] - bacteria_data['centroid-1'].iloc[0]

    plt.plot(dx,dy)
    plt.xlim(-20,20)
    plt.ylim(-20,20)
    plt.xlabel('$\Delta x$ (px)')
    plt.ylabel('$\Delta y$ (px)')

    # Plot the evolution of the area of the contour
    plt.subplot(G[0,2])
    plt.plot(bacteria_data['faketime'], bacteria_data['area'], 'ko--')
    plt.ylabel('area (px²)')

    plt.subplot(G[1,:])
    ## Extract tripes for different channels
    drug_stipe, mask_stripe = bacteria_stipe(drug_img, bacteria_data, max_time_pos,
                                             bacteria_masks=bmasks)
    trp_stipe = bacteria_stipe(trp_img, bacteria_data,  max_time_pos)
    vis_stipe = bacteria_stipe(vis_img, bacteria_data,  max_time_pos)

    # Rescale intensity between percentile
    percentil = (0.1, 99.9)
    ps, pe = np.percentile(drug_stipe, percentil)
    drug_stipe = rescale_intensity(drug_stipe, in_range=(ps, pe))

    ps, pe = np.percentile(trp_stipe, percentil)
    trp_stipe = rescale_intensity(trp_stipe, in_range=(ps, pe))

    ps, pe = np.percentile(vis_stipe, percentil)
    vis_stipe = rescale_intensity(vis_stipe, in_range=(ps, pe))

    # Plot a vertical stack of all those 3 stripes
    plt.imshow(np.vstack((drug_stipe, trp_stipe, vis_stipe)), 'gray')

    ## Plot the contour of the bacteria (we have to convert the label to float32)
    contours = find_contours(mask_stripe == skutil.img_as_float32([label]), 0.5)
    for ct in contours:
        ye, xe = ct.T
        plt.plot(xe, ye, 'C3-', alpha=0.9)

    plt.axis('off')


    # Plot the fluorescence evolution and TRP evolution
    plt.subplot(G[2,:])
    CHdrug_nobg = bacteria_data['intensity_mean-1']-bacteria_data['backgound_mean-1']
    CHtrp_nobg = bacteria_data['intensity_mean-0']-bacteria_data['backgound_mean-0']
    plt.plot(bacteria_data['faketime'], CHdrug_nobg, 'C0')
    plt.ylabel('DRUG', color='C0')

    ax2 = plt.gca().twinx()
    ax2.plot(bacteria_data['faketime'], CHtrp_nobg, 'C1')
    ax2.set_ylabel('TRP', color='C1')

    plt.title('accumulation corrected from background')
    plt.suptitle(f'{expe_name}_Pos-{pos}_Bact-{label}')
    plt.tight_layout()


def create_all_bacteria_summary(experiment_processed_path, dest_folder=None):
    """
    Create the summary figure for a given processed experiment
    """

    f = Path(experiment_processed_path)

    if dest_folder is not None:
        dest_folder = Path(dest_folder)

    # Load bacteria data
    d = pd.read_csv(f/'bacteria_data.csv', index_col=0)

    # Load images and masks
    drug_img = tifffile.imread(f / 'registered_images_drug.tiff')
    trp_img = tifffile.imread(f / 'registered_images_TRP.tiff')
    vis_img = tifffile.imread(f / 'registered_images_vis.tiff')
    bmasks = tifffile.imread(f / 'tracked_bacteria_masks.tiff')

    # Create the name for the experiment with the date
    exp_name = f.parent.parent.name + '-' + f.name

    # Groupby position and bacteria and create the figure
    for gname, gdata in d.groupby(['position', 'label']):

        # The output dir (create it if it does not exist)
        if dest_folder is not None:
            output_dir = dest_folder / f'bacteria_plot/Pos{gname[0]}'
        else:
            output_dir = f / f'bacteria_plot/Pos{gname[0]}'

        output_dir.mkdir(exist_ok=True, parents=True)

        # The name of the figure to save
        outfig = output_dir / f'bact-{gname[1]}.png'

        if not outfig.is_file():
            plot_bacteria_summary(gdata, vis_img[:, gname[0]], drug_img[:, gname[0]], trp_img[:, gname[0]], bmasks[:, gname[0]],
                                  pos = gname[0], label=gname[1], expe_name=exp_name)

            plt.savefig(output_dir / f'bact-{gname[1]}')
            plt.close('all')
        else:
            print(f'{outfig} already exist')
